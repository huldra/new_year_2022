﻿// Copyright (c) <year> Your name

#include "engine/easy.h"

using namespace arctic;  // NOLINT

Font g_font;
Sprite background;
Sprite bot_menu;
Sprite menu_frame;
Sprite smoke;
Sprite backsmoke;
Sprite atlas;
Sprite buttons;

Sound music;
Sound g_button_down;
Sound g_button_up;
Sound g_hit;
Sound g_money_sound;
Sound g_arrow_sound;
Sound g_bear_sound;
Sound g_deer_sound;
Sound g_snow_sound;
Sound g_ultimate_sound;

std::shared_ptr<Panel> gui;

std::vector<std::vector<Sprite>> unit_frame_img;
std::vector<std::vector<Sprite>> freezing_unit_frame_img;
enum UnitType {
  UnitSanta = 0,
  UnitBear = 1,
  UnitSnegurochka = 2,
  UnitElf = 3,
  UnitDeer = 4,
  UnitZombie = 5,
  UnitGhost = 6,
  UnitGhoul = 7,
  UnitFire = 8,
  UnitShoggoth = 9
};

struct Enemy {
  bool is_alive;
  bool is_freezing;
  Vec2F pos;
  Vec2F spawn_pos;
  Vec2F vel;
  UnitType type;
  Ui64 uid;
  bool has_bullet;
  Si32 frame_idx;
  double time_to_next_frame;
};

struct Bullet {
  Vec2F pos;
  Vec2F vel;
  float fall_g;
  size_t target_idx;
  Ui64 target_uid;
  Si32 r;
};

std::vector<std::shared_ptr<Button>> button_vec;
std::vector<Si32> cost_vec = {1, 2, 3, 4, 50, 100};
std::vector<Enemy> enemies;
std::vector<Bullet> bullets;
Ui64 next_uid = 0;
Si64 gold = 0;
Si64 points = 0;

double background_speed = 0.8;
double background_x = 0.0;
double backsmoke_speed = -32.0;
double backsmoke_x = 1050.0;
double smoke_speed = -65.0;
double smoke_x = 0.0;
double fall_g = 9.8 * 15.0;
double enemy_interframe = 1.0/4.0;
double time_to_enemy = 0.0;
double time_to_shoot = 0.01;
double shoot_interval = 3.0;
bool is_elf_enabled = false;
double elf_shot_frame_duration = shoot_interval * 0.5;
double enemy_interval = 1.5;//0.045;
double elf_time_to_idle = 0.0;
double gold_mult = 1.0;
double elf_mult = 1.0;
double enemy_mult = 1.0;
double enemy_exp = 1.15;
double enemy_mult_interval = 5.0;
double time_to_enemy_mult = 1.0;
double last_ground_spawn_time = -1.0;
double deer_time_to_stop = 0.0;
double deer_duration = 1.0;
double deer_mult = 1.0;
bool is_bear_active = false;
float bear_default_x = 55;
float bear_vel = 64.f;
bool is_ultimate_active = false;
double time_to_finish_ultimate = 1.0;
double ultimate_duration = 1.0;
double last_cast_time = -1.0;

bool is_snow_active = false;
double time_to_next_snowflake = 0.0;
double snowflake_interval = 0.2f;
double snowflake_mult = 1.0;

struct PlayerUnit {
  float x;
  UnitType type;
  bool is_alive = true;
};

std::vector<PlayerUnit> player_units;

void NewGame() {
  cost_vec = {1, 2, 3, 4, 50, 100};
  gold = 0;
  points = 0;

  background_speed = 0.8;
  background_x = 0.0;
  backsmoke_speed = -32.0;
  backsmoke_x = 1050.0;
  smoke_speed = -65.0;
  smoke_x = 0.0;
  fall_g = 9.8 * 15.0;
  enemy_interframe = 1.0/4.0;
  time_to_enemy = 0.0;
  time_to_shoot = 0.01;
  shoot_interval = 3.0;
  is_elf_enabled = false;
  elf_shot_frame_duration = shoot_interval * 0.5;
  enemy_interval = 1.5;//0.045;
  elf_time_to_idle = 0.0;
  gold_mult = 1.0;
  elf_mult = 1.0;
  enemy_mult = 1.0;
  enemy_exp = 1.15;
  enemy_mult_interval = 5.0;
  time_to_enemy_mult = 1.0;
  last_ground_spawn_time = -1.0;
  deer_time_to_stop = 0.0;
  deer_duration = 1.0;
  deer_mult = 1.0;
  is_bear_active = false;
  bear_default_x = 55;
  bear_vel = 64.f;
  is_ultimate_active = false;
  time_to_finish_ultimate = 1.0;
  ultimate_duration = 1.0;
  last_cast_time = -1.0;

  is_snow_active = false;
  time_to_next_snowflake = 0.0;
  snowflake_interval = 0.2f;
  snowflake_mult = 1.0;

  enemies.clear();

  player_units.resize(10);
  for (size_t i = 0; i < player_units.size(); ++i) {
    player_units[i].is_alive = false;
    player_units[i].type = (UnitType)i;
    player_units[i].x = 0;
  }
  player_units[UnitSanta].is_alive = true;
  player_units[UnitSanta].x = 10;
  player_units[UnitBear].is_alive = true;
  player_units[UnitBear].x = 55;
  player_units[UnitSnegurochka].is_alive = true;
  player_units[UnitSnegurochka].x = 76;
  player_units[UnitElf].is_alive = true;
  player_units[UnitElf].x = 102;
  player_units[UnitDeer].is_alive = true;
  player_units[UnitDeer].x = 122;

  button_vec.clear();
  gui.reset(new Panel((Ui64)-1, Vec2Si32(0, 0), ScreenSize()));
  for (Si32 i = 0; i < 6; ++i) {
    Sprite disabled;
    disabled.Reference(buttons, 0, 29 * i, 29, 29);
    Sprite normal;
    normal.Reference(buttons, 29, 29 * i, 29, 29);
    Sprite down;
    down.Reference(buttons, 29*3, 29 * i, 29, 29);
    Sprite hovered;
    hovered.Reference(buttons, 29*2, 29 * i, 29, 29);

    std::shared_ptr<Button> button(new Button(i,
      Vec2Si32(377 + i * 34, 13),
      normal, down, hovered, g_button_down, g_button_up, KeyCode(kKey1 + i), i+1, disabled));
    button->SetEnabled(false);
    button_vec.push_back(button);
    gui->AddChild(button);
  }
}

void ShootRandomSnow() {
  Bullet b;
  b.fall_g = (float)fall_g / 2.f;
  b.vel = Vec2F(Random(-100, 100) * 0.01f, 0.f);
  b.pos = Vec2F((float)Random(0, 960), (float)Random(270, 290));
  b.target_idx = 0;
  b.target_uid = 0;
  b.r = 1;
  bullets.push_back(b);
}

void ShootBullet(Vec2F shoot_from, bool do_fall) {
  if (!do_fall) {
    if (Random(0, 100) < 90) {
      ShootRandomSnow();
    }
  }
  if (!enemies.size()) {
    if (!do_fall) {
      ShootRandomSnow();
    }
    return;
  }
  Si64 best_idx = -1;
  float best_x = 2000;
  size_t idx = 0;
  if (!do_fall) {
    idx = (size_t)Random(0, enemies.size()-1);
  }

  for (size_t i = 0; i < enemies.size(); ++i) {
    Enemy &enemy = enemies[idx];
    float metric = enemy.pos.x + (enemy.pos.y > 60 ? 0 : 200);
    if (metric <= best_x && enemy.is_alive && !enemy.has_bullet && enemy.pos.x > shoot_from.x) {
      best_x = metric;
      best_idx = idx;
    }
    idx++;
    if (idx >= enemies.size()) {
      if (best_idx != -1) {
        break;
      }
      idx = 0;
    }
  }
  if (best_idx < 0) {
    if (!do_fall) {
      ShootRandomSnow();
    }
    return;
  }
  Enemy &target = enemies[(size_t)best_idx];
  if (!do_fall) {
    shoot_from.x = target.pos.x + unit_frame_img[target.type][0].Width()/2;
  }
  Vec2F to_pos = target.pos + Vec2F(unit_frame_img[target.type][0].Size()) * 0.5f;
  double dy = to_pos.y - shoot_from.y;
  double speed = do_fall? 270.0 : target.vel.x;
  double total_speed = speed - target.vel.x;
  if (do_fall && total_speed < 1) {
    total_speed = 1;
  }
  double total_distance = abs(to_pos.x - shoot_from.x);
  double total_time = do_fall ? (total_distance / total_speed) : 2.f;
  double up_vel = do_fall ? (total_time * 0.5 * fall_g + dy / std::max(total_time, 0.1)) : 0.f;
  Bullet b;
  b.fall_g = (float)(do_fall ? fall_g : fall_g / 2.f);
  b.vel = Vec2F((float)speed, (float)up_vel);
  b.pos = shoot_from;
  b.target_idx = (size_t)best_idx;
  b.target_uid = target.uid;
  b.r = do_fall ? 2 : 1;
  bullets.push_back(b);
  target.has_bullet = true;
}

void AddEnemy() {
  Enemy e;
  e.is_alive = true;
  e.is_freezing = false;
  e.has_bullet = false;
  e.pos = Vec2F(960, 53);
  e.vel = Vec2F(-32.f + Random(0, 1500) * 0.01f, 0);

  if (Time() - last_ground_spawn_time > 0.2) {
    while (true) {
      e.type = (UnitType)Random(Si32(UnitZombie), Si32(UnitShoggoth));
      if (e.type != UnitGhost && e.type != UnitFire) {
        break;
      }
    }
  } else {
    if (Random(0, 100) < 30) {
      e.type = (UnitType)Random(Si32(UnitZombie), Si32(UnitShoggoth));
    } else {
      if (Random(0, 1) == 1) {
        e.type = UnitGhost;
      } else {
        e.type = UnitFire;
      }
    }
  }
  if (e.type != UnitGhost && e.type != UnitFire) {
    last_ground_spawn_time = Time();
  }

  e.uid = next_uid;
  e.frame_idx = 0;
  e.time_to_next_frame = enemy_interframe;
  next_uid++;
  if (e.type == UnitGhost || e.type == UnitFire) {
    e.pos.y = (float)Random(53+30, 240);
  }
  if (e.type == UnitGhost) {
    e.vel.x *= 2.f;
  }

  e.spawn_pos = e.pos;

  // Add to the array
  for (size_t i = 0; i < enemies.size(); ++i) {
    if (enemies[i].is_alive == false) {
      enemies[i] = e;
      return;
    }
  }
  enemies.push_back(e);
}

void EasyMain() {
  ResizeScreen(960, 270);
  music.Load("data/music.ogg", true);
  music.Play();
  g_button_up.Load("data/button_up.wav", true);
  g_button_down.Load("data/button_down.wav", true);
  g_hit.Load("data/hit.wav", true);
  g_money_sound.Load("data/money.wav", true);
  g_arrow_sound.Load("data/arrow.wav", true);
  g_bear_sound.Load("data/bear.wav", true);
  g_deer_sound.Load("data/deer.wav", true);
  g_snow_sound.Load("data/snow.wav", true);
  g_ultimate_sound.Load("data/ultimate.wav", true);
  //g_font.Load("data/arctic_one_bmf.fnt");
  g_font.LoadLetterBits(g_tiny_font_letters, 8, 8);
  background.Load("data/background.tga");
  bot_menu.Load("data/bot_menu.tga");
  smoke.Load("data/smoke.tga");
  backsmoke.Load("data/smoke.tga");
  atlas.Load("data/atlas.tga");
  buttons.Load("data/buttons.tga");
  menu_frame.Reference(bot_menu, 0, 0, 960, 53);

  unit_frame_img.resize(10);
  freezing_unit_frame_img.resize(10);

  for (size_t i = 0; i < unit_frame_img.size(); ++i) {
    unit_frame_img[i].resize(2);
    freezing_unit_frame_img[i].resize(2);
  }
  unit_frame_img[UnitSanta][0].Reference(atlas, 34, 256-143, 45, 33);
  unit_frame_img[UnitSanta][1].Reference(atlas, 34, 256-143, 45, 33);
  unit_frame_img[UnitSnegurochka][0].Reference(atlas, 0, 256-235, 24, 29);
  unit_frame_img[UnitSnegurochka][1].Reference(atlas, 26, 256-236, 18, 32);
  unit_frame_img[UnitElf][0].Reference(atlas, 1, 256-133, 17, 19);
  unit_frame_img[UnitElf][1].Reference(atlas, 18, 256-132, 17, 19);
  unit_frame_img[UnitBear][0].Reference(atlas, 0, 256-204, 20, 30);
  unit_frame_img[UnitBear][1].Reference(atlas, 22, 256-204, 20, 30);
  unit_frame_img[UnitDeer][0].Reference(atlas, 0, 256-173, 32, 33);
  unit_frame_img[UnitDeer][1].Reference(atlas, 33, 256-172, 38, 28);
  unit_frame_img[UnitZombie][0].Reference(atlas, 0, 256-20, 10, 19);
  unit_frame_img[UnitZombie][1].Reference(atlas, 11, 256-20, 13, 19);
  unit_frame_img[UnitGhost][0].Reference(atlas, 23, 256-39, 21, 18);
  unit_frame_img[UnitGhost][1].Reference(atlas, 45, 256-39, 21, 18);
  unit_frame_img[UnitGhoul][0].Reference(atlas, 24, 256-21, 17, 21);
  unit_frame_img[UnitGhoul][1].Reference(atlas, 41, 256-21, 17, 21);
  unit_frame_img[UnitFire][0].Reference(atlas, 0, 256-70, 25, 29);
  unit_frame_img[UnitFire][1].Reference(atlas, 26, 256-70, 25, 29);
  unit_frame_img[UnitShoggoth][0].Reference(atlas, 0, 256-109, 46, 39);
  unit_frame_img[UnitShoggoth][1].Reference(atlas, 47, 256-109, 46, 39);
  for (size_t i = 0; i < unit_frame_img.size(); ++i) {
    for (size_t j = 0; j < unit_frame_img[i].size(); ++j) {
      freezing_unit_frame_img[i][j].Clone(unit_frame_img[i][j]);
      Sprite s = freezing_unit_frame_img[i][j];
      for (int y = 0; y < s.Height(); ++y) {
        for (int x = 0; x < s.Width(); ++x) {
          Rgba *p = s.RgbaData() + y * s.StridePixels() + x;
          if (p->a) {
            int intensity = ((int)p->r + (int)p->g*2 + (int)p->b)/4;
            p->r = (int)p->r / 8 + (intensity * 7 / 8);
            p->g = (int)p->g / 8 + (intensity * 7 / 8);
            p->b = std::max((Ui8)intensity, p->b)/2+127;
            //p->a = 75;
          }
        }
      }
    }
  }

  for (int y = 0; y < smoke.Height(); ++y) {
    for (int x = 0; x < smoke.Width(); ++x) {
      Rgba *p = smoke.RgbaData() + y * smoke.StridePixels() + x;
      if (p->a) {
        p->a = 75;
      }
      Rgba *p2 = backsmoke.RgbaData() + y * backsmoke.StridePixels() + x;
      if (p2->a) {
        p2->a = 75;
      }
    }
  }


  double prev_time = Time();
  double time = Time();
  double dt = time - prev_time;
  double last_gold_time = time;
  enemies.reserve(10000);

  NewGame();


  while (!IsKeyDownward(kKeyEscape)) {
    if (!music.IsPlaying()) {
      music.Play();
    }
    prev_time = time;
    time = Time();
    dt = time - prev_time;
    if (time - last_gold_time > 1.0) {
      gold += 1;
      last_gold_time += 1.0 / gold_mult;
    }
    //dt*=3;

    Ui64 clicked = Ui64(-1);
    std::deque<GuiMessage> messages;
    for (Si32 idx = 0; idx < InputMessageCount(); ++idx) {
      gui->ApplyInput(GetInputMessage(idx), &messages);
    }
    for (auto it = messages.begin(); it != messages.end(); ++it) {
      if (it->kind == kGuiButtonClick) {
          clicked = it->panel->GetTag();
      }
    }
    size_t clicked_button = (size_t)clicked;
    if (clicked >= 0 && clicked < 6) {
      gold -= cost_vec[clicked_button];
    }
    switch (clicked_button) {
      case 0:
        // Deer
        cost_vec[clicked_button] = cost_vec[clicked_button] * 5;
        deer_mult *= 5;
        deer_time_to_stop = 15;
        g_deer_sound.Play();
        break;
      case 1:
        // elf
        cost_vec[clicked_button] = Si32(cost_vec[clicked_button] * 1.5) + 1;
        if (!is_elf_enabled) {
          is_elf_enabled = true;
        } else {
          elf_mult *= 1.4;
        }
        g_arrow_sound.Play();
        break;
      case 2:
        // Gold
        cost_vec[clicked_button] = Si32(cost_vec[clicked_button] * 1.5) + 5;
        gold_mult *= 1.5;
        g_money_sound.Play();
        break;
      case 3:
        // Bear
        cost_vec[clicked_button] = cost_vec[clicked_button] * 4;
        is_bear_active = true;
        g_bear_sound.Play();
        break;
      case 4:
        // Snow
        cost_vec[clicked_button] = Si32(cost_vec[clicked_button] * 1.5) + 1;
        is_snow_active = true;
        snowflake_mult *= 1.5;
        last_cast_time = time;
        g_snow_sound.Play();
        break;
      case 5:
        // Ultimate
        cost_vec[clicked_button] = cost_vec[clicked_button] * 3;
        is_ultimate_active = true;
        time_to_finish_ultimate = ultimate_duration;
        enemy_mult = enemy_interval * (elf_mult / shoot_interval + snowflake_mult * 2.f / snowflake_interval);
        last_cast_time = time;
        g_ultimate_sound.Play();
        break;

      default:
        break;
    }


    if (!player_units[UnitDeer].is_alive) {
      button_vec[0]->SetVisible(false);
    }
    if (!player_units[UnitElf].is_alive) {
      button_vec[1]->SetVisible(false);
    }
    if (!player_units[UnitSanta].is_alive) {
      button_vec[2]->SetVisible(false);
    }
    if (!player_units[UnitBear].is_alive) {
      button_vec[3]->SetVisible(false);
    }
    if (!player_units[UnitSnegurochka].is_alive) {
      button_vec[4]->SetVisible(false);
      button_vec[5]->SetVisible(false);
    }


    for (size_t i = 0; i < button_vec.size(); ++i) {
      if (gold >= cost_vec[i]) {
        button_vec[i]->SetEnabled(true);
      } else {
        button_vec[i]->SetEnabled(false);
      }
    }
    deer_time_to_stop -= dt;
    if (deer_time_to_stop > 2.0) {
      button_vec[0]->SetEnabled(false);
    }
    if (is_bear_active) {
      button_vec[3]->SetEnabled(false);
    }
    if (is_ultimate_active) {
      button_vec[5]->SetEnabled(false);
    }

    if (player_units[UnitSanta].is_alive) {
      time_to_enemy_mult -= dt;
      if (time_to_enemy_mult < 0.0) {
        time_to_enemy_mult = enemy_mult_interval;
        enemy_mult *= enemy_exp;
      }
    }

    time_to_enemy -= dt;
    while (time_to_enemy < 0.0) {
      AddEnemy();
      time_to_enemy += Random(0, 200) * 0.01 * enemy_interval / enemy_mult * (1.0 + 0.5 * sin(time*0.1));
    }

    if (is_elf_enabled && player_units[UnitElf].is_alive) {
      time_to_shoot -= dt;
      while (time_to_shoot < 0) {
        time_to_shoot += shoot_interval / elf_mult;
        Vec2F shoot_from(106, 62);
        ShootBullet(shoot_from, true);
        elf_time_to_idle = elf_shot_frame_duration;
      }
    }

    Clear();
    background_x += background_speed * dt;
    while (background_x > background.Width()) {
      background_x -= background.Width();
    }
    while (background_x < 0) {
      background_x += background.Width();
    }
    background.Draw((Si32)background_x, 0);
    background.Draw((Si32)background_x - background.Width(), 0);


    backsmoke_x += backsmoke_speed * dt;
    while (backsmoke_x > backsmoke.Width()) {
      backsmoke_x -= backsmoke.Width();
    }
    while (backsmoke_x < 0) {
      backsmoke_x += backsmoke.Width();
    }
    backsmoke.Draw((Si32)backsmoke_x, 48);
    backsmoke.Draw((Si32)backsmoke_x - backsmoke.Width() , 48);

    bot_menu.Draw(0, 0);

    smoke_x += smoke_speed * dt;
    while (smoke_x > smoke.Width()) {
      smoke_x -= smoke.Width();
    }
    while (smoke_x < 0) {
      smoke_x += smoke.Width();
    }
    smoke.Draw((Si32)smoke_x, 53);
    smoke.Draw((Si32)smoke_x - smoke.Width() - 1, 53);

    // Draw characters

    if (is_bear_active) {
      player_units[UnitBear].x += (float)dt * bear_vel;
      if (player_units[UnitBear].x > 960) {
        is_bear_active = false;
        player_units[UnitBear].x = -unit_frame_img[UnitBear][0].Width()-960.f;
      }
    } else {
      if (player_units[UnitBear].x < bear_default_x) {
        player_units[UnitBear].x += (float)dt * bear_vel;
      } else {
        player_units[UnitBear].x = bear_default_x;
      }
    }
    if (is_ultimate_active && player_units[UnitSnegurochka].is_alive) {
      time_to_finish_ultimate -= dt;
      if (time_to_finish_ultimate < 0) {
        time_to_finish_ultimate = 0;
        is_ultimate_active = false;
      }
      double ultimate_part = time_to_finish_ultimate/ultimate_duration;
      double ultimate_x = 960 * ultimate_part;

      for (size_t enemy_idx = 0; enemy_idx < enemies.size(); ++enemy_idx) {
        Enemy &e = enemies[enemy_idx];
        if (e.is_alive) {
          if (e.pos.x > ultimate_x && e.pos.x < ultimate_x + 300) {
            e.is_freezing = true;
          }
        }
      }
    }
    if (is_snow_active && player_units[UnitSnegurochka].is_alive) {
      time_to_next_snowflake -= dt;
      while (time_to_next_snowflake <= 0.0) {
        Vec2F shoot_from(-100, (float)Random(270,290));
        ShootBullet(shoot_from, false);
        time_to_next_snowflake += snowflake_interval / snowflake_mult;
      }
    }




    if (player_units[UnitSanta].is_alive) {
      unit_frame_img[UnitSanta][0].Draw((Si32)player_units[UnitSanta].x, 53);
    }
    if (player_units[UnitBear].is_alive) {
      Si32 bear_frame = 0;
      if (player_units[UnitBear].x != bear_default_x) {
        bear_frame = (Si32)fmod(time * 4.0, 2.0);
      }
      unit_frame_img[UnitBear][bear_frame].Draw((Si32)player_units[UnitBear].x, 53);
    }
    if (player_units[UnitSnegurochka].is_alive) {
      Si32 snegurochka_frame = 0;
      if (time < last_cast_time + 1.f) {
        snegurochka_frame = 1;
      }
      unit_frame_img[UnitSnegurochka][snegurochka_frame].Draw(
          (Si32)player_units[UnitSnegurochka].x, 53);
    }

    if (player_units[UnitElf].is_alive) {
      if (elf_time_to_idle > 0) {
        elf_time_to_idle -= dt;
        unit_frame_img[UnitElf][1].Draw((Si32)player_units[UnitElf].x, 53);
      } else {
        unit_frame_img[UnitElf][0].Draw((Si32)player_units[UnitElf].x, 53);
      }
    }

    if (player_units[UnitDeer].is_alive) {
      if (deer_time_to_stop > 0) {
        unit_frame_img[UnitDeer][1].Draw((Si32)player_units[UnitDeer].x, 53);
      } else {
        unit_frame_img[UnitDeer][0].Draw((Si32)player_units[UnitDeer].x, 53);
      }
    }

    // Move enemies
    {
      float enemy_target_x = 0;
      size_t dmg_unit = 0;
      Vec2F dmg_point = Vec2F(0, 0);
      for (size_t i = 0; i < player_units.size(); ++i) {
        if (player_units[i].is_alive) {
          if (i == UnitBear && is_bear_active) {
            continue;
          }
          enemy_target_x = player_units[i].x;
          dmg_point = Vec2F(player_units[i].x, 53.f + unit_frame_img[i][0].Height());
          dmg_unit = i;
        }
      }
      enemy_target_x -= 20;
      size_t count = enemies.size();
      for (size_t enemy_idx = 0; enemy_idx < count; ++enemy_idx) {
        Enemy &enemy = enemies[enemy_idx];
        if (enemy.is_alive) {
          if (enemy.is_freezing) {
            enemy.vel.y -= float(fall_g * dt);
            if (enemy.pos.y < 0) {
              enemy.is_alive = false;
              points++;
            }
          }
          enemy.pos += enemy.vel * (float)dt;
          if (!enemy.is_freezing) {
            float dist = std::max(0.f, enemy.pos.x - enemy_target_x);
            if (dist < 75.f) {
              if (enemy.pos.y > 53.f) {
                float mul = 1.f - EaseInOutCubic(1.f - dist/75.f);
                if (mul < 0) {
                  mul = 0.f;
                }
                if (mul > 1) {
                  mul = 1;
                }
                enemy.pos.y = 53.f + (enemy.spawn_pos.y - 53.f) * mul;
              }
              if (enemy.pos.x < dmg_point.x && enemy.pos.y < dmg_point.y) {
                enemy.is_freezing = true;
                player_units[dmg_unit].is_alive = false;
                g_hit.Play();

              }
            }
            enemy.time_to_next_frame -= dt;
            if (enemy.time_to_next_frame <= 0) {
              enemy.frame_idx = (enemy.frame_idx + 1) % unit_frame_img[enemy.type].size();
              enemy.time_to_next_frame = enemy_interframe;
            }
          }
        }
      }
    }
    // Draw enemies
    {
      size_t count = enemies.size();
      for (size_t enemy_idx = 0; enemy_idx < count; ++enemy_idx) {
        Enemy &enemy = enemies[enemy_idx];
        if (enemy.is_alive) {
          if (enemy.is_freezing) {
            freezing_unit_frame_img[enemy.type][enemy.frame_idx].Draw(Vec2Si32(enemy.pos));
          } else {
            unit_frame_img[enemy.type][enemy.frame_idx].Draw(Vec2Si32(enemy.pos));
          }
        }
      }
    }

    // Draw bullets
    {
      for (size_t bullet_idx = 0; bullet_idx < bullets.size(); ++bullet_idx) {
        Bullet &bullet = bullets[bullet_idx];
        bullet.pos += bullet.vel * (float)dt + Vec2F(0, -bullet.fall_g) * (float)(dt * dt) * 0.5f;
        bullet.vel += Vec2F(0, -bullet.fall_g) * (float)dt;
        if (bullet.target_idx < enemies.size() && bullet.target_uid == enemies[bullet.target_idx].uid) {
          Enemy &enemy = enemies[bullet.target_idx];
          Vec2F enemy_size = Vec2F(unit_frame_img[enemy.type][0].Size());
          if (abs(bullet.pos.y - (enemy.pos.y + enemy_size.y/2)) <= enemy_size.y/2
              && abs(bullet.pos.x - enemy.pos.x - enemy_size.x/2) <= enemy_size.x/2) {
            enemy.is_alive = false;
            points++;
            bullets[bullet_idx] = bullets.back();
            bullets.pop_back();
            --bullet_idx;
          } else if (bullet.pos.y < 0) {
            enemy.has_bullet = false;
            bullets[bullet_idx] = bullets.back();
            bullets.pop_back();
            --bullet_idx;
          }
        } else {
          if (bullet.pos.y < 0) {
            bullets[bullet_idx] = bullets.back();
            bullets.pop_back();
            --bullet_idx;
          }
        }
      }
      for (size_t bullet_idx = 0; bullet_idx < bullets.size(); ++bullet_idx) {
        Bullet &bullet = bullets[bullet_idx];
        DrawCircle(Vec2Si32(bullet.pos), bullet.r, Rgba(255, 255, 255, 255));
      }
    }
    // kill enemies with the bear
    if (is_bear_active && player_units[UnitBear].is_alive) {
      float kill_x = player_units[UnitBear].x + unit_frame_img[UnitBear][0].Width()/2.f;
      float kill_y = 53.f + unit_frame_img[UnitBear][0].Height();
      for (size_t enemy_idx = 0; enemy_idx < enemies.size(); ++enemy_idx) {
        Enemy &e = enemies[enemy_idx];
        if (e.is_alive) {
          if (e.pos.x < kill_x && e.pos.x >= player_units[UnitBear].x && e.pos.y < kill_y) {
            e.is_alive = false;
            points++;
          }
        }
      }
    }
    if (deer_time_to_stop > 0 && player_units[UnitDeer].is_alive) {
      unit_frame_img[UnitDeer][1].Draw((Si32)player_units[UnitDeer].x, 53);
      float kill_x = player_units[UnitDeer].x + unit_frame_img[UnitDeer][0].Width()- 5.f;
      float kill_x_min = kill_x - 5.f;
      float kill_y = 53.f + unit_frame_img[UnitDeer][0].Height();
      for (size_t enemy_idx = 0; enemy_idx < enemies.size(); ++enemy_idx) {
        Enemy &e = enemies[enemy_idx];
        if (e.is_alive) {
          if (e.pos.x < kill_x && e.pos.x >= kill_x_min && e.pos.y < kill_y) {
            e.is_alive = false;
            points++;
          }
        }
      }
    }

    menu_frame.Draw(0, 0);

    // Draw HUD
    gui->Draw(Vec2Si32(0, 0));

    for (Si32 i = 0; i < 6; ++i) {
      Vec2Si32 pos(377 + i * 34 + 3, 13 + 3);
      char price[128];
      sprintf(price, u8"%i", cost_vec[i]);
      for (Si32 x = -1; x < 2; ++x) {
        for (Si32 y = -1; y < 2; ++y) {
          g_font.Draw(price, pos.x + x, pos.y + y,
            kTextOriginBottom,
            kDrawBlendingModeColorize,
            kFilterNearest,
            Rgba(0xff000000));
        }
      }
      g_font.Draw(price, pos.x, pos.y,
        kTextOriginBottom,
        kDrawBlendingModeColorize,
        kFilterNearest,
        Rgba(0xffffffff));
    }

    if (!player_units[UnitSanta].is_alive) {
      for (Si32 x = -1; x < 2; ++x) {
        for (Si32 y = -1; y < 2; ++y) {
          g_font.Draw(u8"     Дед Мороз повержен!\nЖми ПРОБЕЛ, попробуй снова!",
                      ScreenSize().x/2-75+x, ScreenSize().y*2/3+y,
                      kTextOriginBottom,
                      kDrawBlendingModeColorize,
                      kFilterNearest,
                      Rgba(0,0,0, 255));
        }
      }
      g_font.Draw(u8"     Дед Мороз повержен!\nЖми ПРОБЕЛ, попробуй снова!",
                  ScreenSize().x/2-75, ScreenSize().y*2/3,
        kTextOriginBottom,
        kDrawBlendingModeColorize,
        kFilterNearest,
        Rgba((Ui8)Random(200,255), (Ui8)Random(200,255), (Ui8)Random(200,255), 255));

      if (IsKeyDown(kKeySpace)) {
        NewGame();
      }
    }

    char score[128];
    sprintf(score, u8"Золото: %lli\nСчет: %lli", gold, points);
    g_font.Draw(score, 10, 270-20);


    ShowFrame();
  }
}
